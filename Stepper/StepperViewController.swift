//
//  StepperViewController.swift
//  Stepper
//
//  Created by Eryn Wells on 1/16/15.
//  Copyright (c) 2015 Eryn Wells. All rights reserved.
//

import HealthKit
import UIKit


class StepperViewController: UIViewController
{
    var healthAccessor: HealthKitAccessor

    required init(coder: NSCoder)
    {
        self.healthAccessor = HealthKitAccessor.sharedHealthAccessor
        super.init(coder: coder)
    }
}
