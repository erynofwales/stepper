//
//  HealthKitAccessor.swift
//  Stepper
//
//  Created by Eryn Wells on 1/17/15.
//  Copyright (c) 2015 Eryn Wells. All rights reserved.
//

import HealthKit
import UIKit


let HealthKitAccessor_DidUpdateStepCountNotification = "HealthKitAccessorDidUpdateStepCountNotification"
let HealthKitAccessor_StepCountKey = "stepCount"


private var sHealthAccessor: HealthKitAccessor?


class HealthKitAccessor: NSObject
{
    class var sharedHealthAccessor: HealthKitAccessor
    {
        if let healthAccessor = sHealthAccessor {
            return healthAccessor
        }
        sHealthAccessor = HealthKitAccessor(healthStore: HKHealthStore())
        return sHealthAccessor!
    }


    /**
     * The current step count for today. This gets updated each time updateStepCountForToday is called, either by an 
     * update from the observer query, or by clients.
     */
    var stepCountForToday: UInt? = nil

    /** A HealthKit health store, a handle to the HealthKit database. */
    private var healthStore: HKHealthStore

    /** Flag indicating whether HealthKit authorization has been requested. */
    private var isAuthorized: Bool = false

    private var stepCountObserverQuery: HKObserverQuery?


    /** 
     * Flag indicating whether HealthKit is available on this system.
     * This is a light wrapper around the class HKHealthStore method of the same name.
     */
    private var isHealthDataAvailable: Bool
    {
        return HKHealthStore.isHealthDataAvailable()
    }


    init(healthStore: HKHealthStore)
    {
        self.healthStore = healthStore
        super.init()
    }


    /**
     * Request authorization from the user for access to the various bits of HealthKit we need.
     *
     * @param [in] completion A completion block, called when the request finishes. Optional.
     */
    func requestAuthorization(completion: ((success: Bool, error: NSError!) -> Void)? = nil)
    {
        if !self.isHealthDataAvailable {
            return
        }

        let shareTypes = NSSet()

        let readTypes = NSSet(objects:
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierStepCount))

        self.healthStore.requestAuthorizationToShareTypes(shareTypes, readTypes: readTypes) {
            (success: Bool, error: NSError!) -> Void in
            if !success {
                NSLog("HealthKit authorization request failed: \(error.localizedDescription)")
            } else {
                NSLog("HealthKit authorization succeeded")
            }
            self.isAuthorized = success
            if let comp = completion {
                comp(success: success, error: error)
            }
        }
    }


    func startObservingStepCountForToday()
    {
        if !self.isAuthorized {
            self.requestAuthorization() {
                (success: Bool, error: NSError!) in
                if success {
                    self.doObserverQuery()
                }
            }
            return
        }

        if self.stepCountObserverQuery != nil {
            // Query already active. Nothing to do.
            return
        }

        doObserverQuery()
    }


    private func doObserverQuery()
    {
        let stepCount = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierStepCount)

        let query = HKObserverQuery(sampleType: stepCount, predicate: self.predicateForToday()) {
            (query: HKObserverQuery!, completion: HKObserverQueryCompletionHandler!, error: NSError!) in
            if error != nil {
                NSLog("Error observing step count: \(error.localizedDescription)")
                return
            }
            self.updateStepCountForToday()
        }
        self.healthStore.executeQuery(query)
    }


    func stopObservingStepCountForToday()
    {
        if let query = self.stepCountObserverQuery {
            self.healthStore.stopQuery(query)
            self.stepCountObserverQuery = nil
        }
    }


    func updateStepCountForToday(completion: ((stepCount: UInt!, error: NSError!) -> Void)? = nil)
    {
        let stepCount = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierStepCount);
        let today = self.predicateForToday()
        let options = HKStatisticsOptions.CumulativeSum

        let query = HKStatisticsQuery(quantityType: stepCount, quantitySamplePredicate: today, options: options) {
            (query: HKStatisticsQuery!, stats: HKStatistics!, error: NSError!) in
            if error != nil {
                NSLog("Error querying HealthKit for step count: \(error.localizedDescription)")
                if let comp = completion {
                    comp(stepCount: nil, error: error)
                }
                return
            }
            if let sum = stats!.sumQuantity() {
                self.stepCountForToday = UInt(sum.doubleValueForUnit(HKUnit.countUnit()))
                let userInfo: NSDictionary = [
                    HealthKitAccessor_StepCountKey: NSNumber(unsignedLong: self.stepCountForToday!),
                ]
                NSNotificationCenter.defaultCenter().postNotificationName(
                    HealthKitAccessor_DidUpdateStepCountNotification,
                    object: self, userInfo: userInfo)
            }
        }

        self.healthStore.executeQuery(query)
    }


    /**
     * Create and return an NSPredicate that spans today.
     *
     * @return An NSPredicate that limits results to today.
     */
    private func predicateForToday() -> NSPredicate
    {
        let calendar = NSCalendar.currentCalendar()
        let startOfToday = calendar.startOfDayForDate(NSDate())
        let endOfToday = calendar.dateByAddingUnit(NSCalendarUnit.CalendarUnitDay, value: 1, toDate: startOfToday, options: NSCalendarOptions.allZeros)
        return HKQuery.predicateForSamplesWithStartDate(startOfToday, endDate: endOfToday, options: HKQueryOptions.StrictStartDate)
    }
}
