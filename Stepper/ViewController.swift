//
//  ViewController.swift
//  Stepper
//
//  Created by Eryn Wells on 1/12/15.
//  Copyright (c) 2015 Eryn Wells. All rights reserved.
//

import HealthKit
import UIKit


class ViewController: StepperViewController
{
    @IBOutlet weak var stepsLabel: UILabel!


    override func viewDidAppear(animated: Bool)
    {
        NSNotificationCenter.defaultCenter().addObserver(self,
            selector: "updateStepsLabel:",
            name: HealthKitAccessor_DidUpdateStepCountNotification,
            object: nil)
        self.healthAccessor.startObservingStepCountForToday()
    }


    override func viewDidDisappear(animated: Bool)
    {
        NSNotificationCenter.defaultCenter().removeObserver(self,
            name: HealthKitAccessor_DidUpdateStepCountNotification,
            object: nil)
    }


    func updateStepsLabel(note: NSNotification)
    {
        if let userInfo = note.userInfo {
            dispatch_async(dispatch_get_main_queue()) {
                let stepCount: NSNumber = userInfo[HealthKitAccessor_StepCountKey]! as NSNumber
                self.stepsLabel.text = String(stepCount.integerValue)
            }
        }
    }
}

