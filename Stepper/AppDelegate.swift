//
//  AppDelegate.swift
//  Stepper
//
//  Created by Eryn Wells on 1/12/15.
//  Copyright (c) 2015 Eryn Wells. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{
    var window: UIWindow?
}

